<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Address</title>
</head>
<body>
    <style>
        body {
            margin: 10px;
            padding: 10px;
            width: auto;
        }
        .container {
            margin-top: 20px;
        }
        label {
            display: inline-block;
            width: 100px;
            text-align: right;
        }
        button{
            margin-left: 220px;
            margin-top: 5px;
        }
    </style>
<?php
require 'config.php';

if(isset ($_POST['fname']) && isset($_POST['lname']) && isset($_POST['email']) && isset($_POST['add1']) 
    && isset($_POST['add2']) && isset($_POST['city']) && isset($_POST['province']) && isset($_POST['zip'])) {
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['email'];
    $add1 = $_POST['add1'];
    $add2 = $_POST['add2'];
    $city = $_POST['city'];
    $province = $_POST['province'];
    $zip = $_POST['zip'];
    $db = new Connection();
    $msg = $db->insert($fname, $lname, $email, $add1, $add2, $city, $province, $zip);
}

?>   
    <h2>New Name</h2>
    <div class="container">
        <?php 
            if(!empty($msg)) {
                echo "<p>" . $msg . "</p>";
            }
        ?>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <label for="fname">Firstname:</label>
            <input type="text" name="fname" ><br>
            <label for="lname">Lastname:</label>
            <input type="text" name="lname" ><br>
            <label for="email">Email:</label>
            <input type="text" name="email" ><br>
            <label for="add1">Address1:</label>
            <input type="text" name="add1" ><br>
            <label for="add2">Address2:</label>
            <input type="text" name="add2" ><br>
            <label for="city">City:</label>
            <input type="text" name="city" ><br>
            <label for="province">Province:</label>
            <input type="text" name="province" ><br>
            <label for="zip">Zip:</label>
            <input type="text" name="zip" ><br>
            <button type="submit">Create</button>
        </form>
    </div>
    <div>
    <a href="index.php">Home</a>
    </div>


</body>
</html>