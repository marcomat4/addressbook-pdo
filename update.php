<?php
require 'config.php';
//fetching data to the fields
$db = new Connection();
$connection = $db->openConnection();
$id = (isset($_GET['id']) ? $_GET['id'] : '');
$sql = 'SELECT * FROM address WHERE id=:id';
$statement = $connection->prepare($sql);
$statement->execute([':id' => $id ]);
$info = $statement->fetch(PDO::FETCH_OBJ);

//update

if(isset($_POST['fname'])  && isset($_POST['lname']) && isset($_POST['id'])) {
    //$pid = $id;
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['email'];
    $address1 = $_POST['add1'];
    $address2 = $_POST['add2'];
    $city = $_POST['city'];
    $province = $_POST['province'];
    $zip = $_POST['zip'];
    $pid = $_POST['id'];
    $msg = $db->update($fname, $lname, $email, $address1, $address2, $city, $province, $zip, $pid);
    if($msg) {
        header('Location: /pdo');
    
    } else {
        echo $msg;
    }
}


 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Address</title>
</head>
<body>
    <style>
        body {
            margin: 10px;
            padding: 10px;
            width: auto;
        }
        .container {
            margin-top: 20px;
        }
        label {
            display: inline-block;
            width: 100px;
            text-align: right;
        }
        button{
            margin-left: 220px;
            margin-top: 5px;
        }
    </style> 
    <h2>Update Info</h2>
    <div class="container">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <label for="fname">Firstname:</label>
            <input value="<?= $info->fname; ?>" type="text" name="fname"><br>
            <label for="lname">Lastname:</label>
            <input value="<?= $info->lname; ?>" type="text" name="lname" ><br>
            <label for="email">Email:</label>
            <input value="<?= $info->email; ?>" type="text" name="email" ><br>
            <label for="add1">Address1:</label>
            <input value="<?= $info->address1; ?>" type="text" name="add1" ><br>
            <label for="add2">Address2:</label>
            <input value="<?= $info->address2; ?>" type="text" name="add2" ><br>
            <label for="city">City:</label>
            <input value="<?= $info->city; ?>" type="text" name="city" ><br>
            <label for="province">Province:</label>
            <input value="<?= $info->province; ?>" type="text" name="province" ><br>
            <label for="zip">Zip:</label>
            <input value="<?= $info->zip; ?>" type="text" name="zip" ><br>
            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
            <button type="submit" name="update">Update</button>
        </form>
    </div>
    <div>
    <a href="index.php">Home</a>
    </div>


</body>
</html>