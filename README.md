# addressbook-pdo

This project is about using PHP Data Object (PDO) in accessing the database.  
---
### Create functionality for *Create*, *Read*, *Update*, *Delete*.

These are the columns needed for the address table:
* firstname
* lastname
* email
* address1
* address2
* city
* province
* zipcode

You can also refer to the database [queries](https://gitlab.com/marcomat4/addressbook-pdo/blob/master/db.sql) that is inside my repository.
**Note: ** *please don't forget to hit the star button if you clone this repository for your own purposes*
