CREATE DATABASE addressbook;
use addressbook;

CREATE TABLE address (
    id int AUTO_INCREMENT primary key,
    fname varchar(255),
    lname varchar(255),
    emai varchar(255),
    address1 varchar (255),
    address2 varchar (255),
    city varchar(100),
    province varchar(255),
    zip varchar(100)
)

