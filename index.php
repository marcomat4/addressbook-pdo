<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Address Book</title>
</head>
<body>
<style>
    body {
        margin: 10px;
        padding: 10px;
        width: auto;
    }
    /* tr.highlight td {padding-right: 30px;} */
    tr.headtable th{ padding-right: 120px;}
</style>
<?php 
require 'config.php';

$db = new Connection();
$infos = $db->select();
?>
    <h2>NAMES</h2>
    <a href="create.php">create new</a><br><br>  
    <div class="container">
        <table>
            <tr class="headtable">
                <th>Name</th>
                <th>Action</th>
            </tr>
            <?php foreach ($infos as $info):  ?>
            <tr class="highlight">
                <td class=""><?php echo $info->fname . " " . $info->lname ?></td>
                <td>
                    <a href="update.php?id=<?= $info->id?>">Edit</a>
                    <a href="delete.php?id=<?= $info->id?>">Delete</a>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
    
</body>
</html>