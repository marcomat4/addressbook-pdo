<?php
class Connection {

    private  $server = "mysql:host=localhost;dbname=addressbook";
    private  $user = "root";
    private  $pass = "";    
    private $options  = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,);
    protected $con;
    
    public function openConnection() {
        try {
            $this->con = new PDO($this->server, $this->user,$this->pass,$this->options);
            return $this->con;
        } catch(PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
    public function closeConnection() {
        $this->con = null;
    }
    // $fname, $lname, $email, $add1, $add2, $city, $province, $zip
    function insert($fname, $lname, $email, $add1, $add2, $city, $province, $zip) {
        $msg = '';
        try {
            $conn = $this->openConnection();
            $sql = 'INSERT INTO address (fname,lname,email,address1,address2,city,province,zip) VALUES (:fname,:lname,:email,:add1,:add2,:city,:province,:zip)';
            $stmt = $conn->prepare($sql);
            // inserting a record
            $stmt->execute(array(':fname' => $fname, ':lname' => $lname , ':email' => $email , ':add1' => $add1, ':add2' => $add2, ':city' => $city, ':province' => $province, 'zip'=>$zip));
            $msg = "New record created successfully!";
        } catch(PDOException $e) {
            //echo "There is some problem in connection: " . $e->getMessage();
        }
        return $msg;
    }
    function select() {
        try {
            $conn = $this->openConnection();
            $sql = 'SELECT * FROM address';
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $infos = $stmt->fetchAll(PDO::FETCH_OBJ);
            return $infos;
        } catch(PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    function fetchSingle($id) {
        $conn = $this -> openConnection();
        $sql = 'SELECT * FROM address WHERE id = :id';
        $stmt = $conn -> prepare($sql);
        $stmt -> execute([':id' => $id]);
        $info = $stmt->fetch(PDO::FETCH_OBJ);
        return $info;
    }

    // $lname, $email, $add1, $add2, $city, $province, $zip, 
    //lname=:lname, email=:email, address1=:address1, address2=:address2, city=:city, province=:province, zip=:zip
    function update($fname,$lname, $email, $add1, $add2, $city, $province, $zip, $id) {
        $success = false;
        try {
           // $msg = false;
           $conn = $this -> openConnection();
            // , email=:email, address1=:add1, address2=:add2, city=:city, province=:province, zip=:zip
            $sql = 'UPDATE address SET fname=:fname, lname=:lname, email=:email, address1=:add1, address2=:add2, city=:city, province=:province, zip=:zip WHERE id=:id';
            $stmt = $conn->prepare($sql);
            // $stmt->bindparam(':id', $id);
            // $stmt->bindparam(':fname', $fname);
            // $stmt->bindparam(':lname', $lname);
            // $stmt->bindparam(':email', $email);
            // $stmt->bindparam(':add1', $add1);
            // $stmt->bindparam(':add2', $add2);
            // $stmt->bindparam(':city', $city);
            // $stmt->bindparam(':province', $province);
            // $stmt->bindparam(':zip', $zip);
            
            $array = array(':fname' => $fname, ':lname' => $lname , ':email' => $email , ':add1' => $add1, 
                    ':add2' => $add2, ':city' => $city, ':province' => $province, ':zip'=>$zip, ':id'=>$id);
            if($stmt -> execute($array)) {
                $success = true;
            }
        }catch(PDOException $e) {
            echo 'Error Update: '. $e;
        }
        return $success;
    }
    function delete($id) {
        $success = false;
        try {
           
            $conn = $this->openConnection();
            $sql = "DELETE FROM address WHERE id=:id";
            $stmt = $conn -> prepare($sql);
            if($stmt->execute([':id' => $id])) {
                $success = true;
            }
        } catch(PDOException $e) {
            //error
        }
        return $success;
    }

}

// $db = new Connection();
// $msg = $db->delete(11);
// if($msg){
//     echo 'DELETED!';
// }else{
//     echo 'ERROR !';
// }
// $msg = $db->insert();
// $db->select();
// echo $msg;


// $db = new Connection();
// $id = 1;
// $fname = 'Marcong';
// $lname = 'Miguely';
// $email = 'Miguely@gmail.com';
// $add1 = 'Miguely Street';
// $add2 = 'Chicago';
// $city = 'Dekalb';
// $prov = 'Forrow';
// $zip = '2020';
// $msg = $db->update($fname, $lname, $email, $add1, $add2, $city, $prov, $zip, $id);
// if($msg){
//     echo 'UPDATED!';
// }else{
//     echo 'ERROR UPDATE!';
// }
?>