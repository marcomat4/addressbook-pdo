<?php 
require 'config.php';
$id = $_GET['id'];
$db = new Connection();
$msg = $db->delete($id);
if($msg) {
    header("Location: /pdo");
    echo 'YES, DELETED!';
    exit();
} else {
    echo 'Error deleting the data';
}

?>